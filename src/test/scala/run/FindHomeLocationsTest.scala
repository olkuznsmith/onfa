package run

import org.junit.Assert._
import org.junit.Test
import run.KMeansLocal.Point

class FindHomeLocationsTest {

  @Test
  def testClusterizeOnePoint(): Unit = {

    val points = List(Point(2.0, 1.0))

    val homePoint: Point = FindHomeLocations.clusterize(points)

    assertEquals(Point(2.0, 1.0), homePoint)
  }

  @Test
  def testClusterizeMultiplePoints(): Unit = {

    val points = List(Point(0.0, 1.0), Point(1.0, 0.0), Point(0.0, -1.0), Point(-1.0, 0.0), Point(0.0, 50))

    val homePoint: Point = FindHomeLocations.clusterize(points)

    assertEquals(Point(0.0, 1.0), homePoint)
  }
}