package run

object KMeansLocal {

  case class Point(dx: Double, dy: Double) extends Serializable {
    val x: Double = dx
    val y: Double = dy

    override def toString: String = "(" + x + ", " + y + ")"

    def dist(p: Point): Double = (x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)
  }

  def clusterMean(points: List[Point]): Point = {
    val cumulative = points.reduceLeft((a: Point, b: Point) => new Point(a.x + b.x, a.y + b.y))
    new Point(cumulative.x / points.length, cumulative.y / points.length)
  }

  def render(points: Map[Int, List[Point]]) {
    for (clusterNumber <- points.keys.toSeq.sorted) {
      println("  Cluster " + clusterNumber)

      val meanPoint = clusterMean(points(clusterNumber))
      println("  Mean: " + meanPoint)

      for (j <- points(clusterNumber).indices) {
        println("    " + points(clusterNumber)(j) + ")")
      }
    }
  }

  def clusters(k: Int, points: List[Point]) =
    points.zipWithIndex
      .groupBy(
        x => x._2 % k) transform (
      (i: Int, p: List[(Point, Int)]) => for (x <- p) yield x._1)

  //  System.out.println("Initial State: ")
  //  render(clusters)

  def iterate(points: List[Point], clusters: Map[Int, List[Point]]): (Map[Int, Point], Map[Int, List[Point]]) = {

    // find cluster means
    val means = (clusters: Map[Int, List[Point]]) =>
      for (clusterIndex <- clusters.keys)
        yield (clusterIndex, clusterMean(clusters(clusterIndex)))

    // find the closest index
    def closest(p: Point, means: Iterable[(Int, Point)]): Int = {
      val distances = for (center <- means) yield (center._1, p.dist(center._2))
      distances.minBy((tuple: (Int, Double)) => tuple._2)._1
    }

    val centroids = means(clusters)

    // assignment step
    val newClusters = points.groupBy((p: Point) => closest(p, centroids))

    //render(newClusters)
    (centroids.toMap, newClusters)
  }

  def run(points: List[Point], k: Int, numIterations: Int): (Map[Int, Point], Map[Int, List[Point]]) = {
    var clusterToTest = clusters(k, points)

    var runResult = iterate(points, clusterToTest)
    for (i <- 2 to numIterations) {
      runResult = iterate(points, runResult._2)
    }
    // render(runResult._2)

    runResult
  }

  def error(centroids: Map[Int, Point], clusters: Map[Int, List[Point]], m: Int): Double = {
    clusters.zip(centroids)
      .map((e: ((Int, List[Point]), (Int, Point))) => {
        val pointCoords = e._1._2
        val centroid = e._2._2
        pointCoords.map((p: Point) => p.dist(centroid)).sum
      })
      .sum / m
  }
}