package run

import java.time.{Instant, LocalDateTime, ZoneOffset}

import org.apache.spark.{SparkConf, SparkContext}
import run.KMeansLocal.Point

object FindHomeLocations {

  case class Entry(deviceId: String, timestamp: Long, location: Point) extends Serializable

  def main(args: Array[String]) {

    //System.setProperty("hadoop.home.dir", "C:\\winutils")

    val sparkConf: SparkConf = new SparkConf
    sparkConf.setAppName("test").setMaster("local")

    val sparkContext: SparkContext = new SparkContext(sparkConf)

    val textFile = sparkContext.textFile("data.csv")

    textFile
      .zipWithIndex()
      .filter(_._2 > 0)
      .map(_._1)
      .map((s: String) => createEntryFromArray(s.split(",")))
      .filter((e: Entry) => onlyNightHours(e))
      .map((e: Entry) => (e.deviceId, e))
      .groupByKey()
      .map((tuple: (String, Iterable[Entry])) => (tuple._1, clusterize(tuple._2.map(_.location).toList)))
      .saveAsTextFile("output_" + System.nanoTime())
  }

  def onlyNightHours(e: Entry): Boolean = {
    val dateTime = toLocalDate(e.timestamp)
    dateTime.getHour >= 20 && dateTime.getHour <= 23 || dateTime.getHour >= 0 && dateTime.getHour <= 10
  }

  def createEntryFromArray(parts: Array[String]): Entry = {
    new Entry(parts(0), parts(1).toLong, new Point(parts(2).toLong, parts(3).toLong))
  }

  def clusterize(points: List[Point]): Point = {

    var minError = 10000000.0
    var bestClusters: (Map[Int, Point], Map[Int, List[Point]]) = null
    var bestK = -1

    for (k <- 1 to 20) {
      val clusters = KMeansLocal.run(points, k, 20)
      val err = KMeansLocal.error(clusters._1, clusters._2, points.size)
      if (err < minError) {
        minError = err
        bestClusters = clusters
        bestK = k
      }
    }

    println("bestK=" + bestK)

    val bestClusterIndex = bestClusters._2.maxBy((tuple: (Int, List[Point])) => tuple._2.size)._1
    val homeLocation = bestClusters._1(bestClusterIndex)

    homeLocation
  }

  def toLocalDate(datetime: Long): LocalDateTime = {
    Instant.ofEpochMilli(datetime).atZone(ZoneOffset.ofHours(3)).toLocalDateTime
  }
}
